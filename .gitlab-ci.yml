stages:
  - prepare
  - build
  - deploy


default:
  image: ruby:3.0.2
  interruptible: true # All jobs are interruptible by default
  # The following 'retry' configuration settings may help avoid false build failures
  #  during brief problems with CI/CD infrastructure availability

variables:
  # NO_CONTRACTS speeds up middleman builds
  NO_CONTRACTS: 'true'


### COMMON JOBS REUSED VIA `extends`:

.ruby-cache:
  cache:
    key: "ruby-3.0"
    policy: pull
    paths:
      - vendor

.bundle-install:
  extends: .ruby-cache
  before_script:
    - gem install bundler --no-document
    - bundle config set path 'vendor'
    - bundle install --quiet --jobs 4

###################################
#
# PREPARE STAGE
#
###################################

ruby-push-cache:
  extends: .bundle-install
  stage: prepare
  rules:
    # Only run this job from pipelines for the default branch
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  cache:
    # Only push the cache from this job, to save time on all other jobs.
    policy: pull-push
  script:
    - echo "Pushing updated ruby cache..."

###################################
#
# BUILD STAGE
#
###################################

build:
  extends: .bundle-install
  stage: build
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
  script:
    - bundle exec middleman build --bail

###################################
#
# DEPLOY STAGE
#
###################################

# NOTE: In order to keep the pipeline as fast as possible for the default branch
#       we combine both the build and deploy to GitLab pages in a single job.
pages:
  extends: .bundle-install
  stage: deploy
  rules:
    # Only run the deploy on the default branch. If you want to deploy from Merge Request branches,
    # you can use Review Apps: https://docs.gitlab.com/ee/ci/review_apps
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  needs: [] # Prevents job from being blocked by jobs in previous stages.
  script:
    - bundle exec middleman build --bail
  artifacts:
    paths:
      - public
